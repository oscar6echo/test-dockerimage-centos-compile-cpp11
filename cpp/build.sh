g++ -g -Wall -fexceptions -c -std=c++1y ./src/main.cpp -o ./obj/main.o
g++ -g -Wall -fexceptions -c -std=c++1y ./src/Graph.cpp -o ./obj/Graph.o
g++ -g -Wall -fexceptions -c -std=c++1y ./src/Dijkstra.cpp -o ./obj/Dijkstra.o
g++ -g -Wall -fexceptions -c -std=c++1y ./src/Prim.cpp -o ./obj/Prim.o
g++ ./obj/main.o ./obj/Graph.o ./obj/Dijkstra.o ./obj/Prim.o  -o ./bin/main.out

