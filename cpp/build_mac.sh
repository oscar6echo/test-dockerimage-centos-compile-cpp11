g++ -g -Wall -fexceptions -c -std=c++11 ./src/main.cpp -o ./obj/main.o
g++ -g -Wall -fexceptions -c -std=c++11 ./src/Graph.cpp -o ./obj/Graph.o
g++ -g -Wall -fexceptions -c -std=c++11 ./src/Dijkstra.cpp -o ./obj/Dijkstra.o
g++ -g -Wall -fexceptions -c -std=c++11 ./src/Prim.cpp -o ./obj/Prim.o
g++ ./obj/main.o ./obj/Graph.o ./obj/Dijkstra.o ./obj/Prim.o  -o ./bin/main.out
