
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    cout << "Testing some c++11 things...\n"
         << endl;

    vector<int> myvector;
    for (int i = 1; i <= 5; i++)
        myvector.push_back(i);

    cout << "myvector contains:" << endl;

    //use auto instead of the long-winded vector::iterator
    for (auto it = myvector.begin(); it != myvector.end(); ++it)
        cout << *it << endl;

    return 0;
}
