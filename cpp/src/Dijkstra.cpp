
#include <iostream>
#include <iomanip>

#include "Graph.hpp"
#include "Dijkstra.hpp"

using namespace std;

Dijkstra::Dijkstra(Graph arg_graph) : graph(arg_graph),
                                      min_dist(arg_graph.getNbVertex()),
                                      previous(arg_graph.getNbVertex()){};

void Dijkstra::init(vertex_t arg_source)
{
    while (!node_heap.empty())
    {
        node_heap.pop();
    }

    for (int u = 0; u < graph.getNbVertex(); ++u)
    {
        // Maximum value for distance ~ INFINITY
        min_dist[u] = graph.getNbEdge() * graph.getMaxEdgeDist() + 1;
        // min_dist[u] = graph.getNbEdge() * graph.getNbVertex() + 1;
        previous[u] = -1;
    }

    // source initialisation
    source = arg_source;
    min_dist[arg_source] = 0;
}

Dijkstra::~Dijkstra()
{
}

// see https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
void Dijkstra::buildShortestPathTree(vertex_t source)
{
    init(source);

    node_heap.push(node(source, min_dist[source]));

    while (!node_heap.empty())
    {
        node current_node = node_heap.top();
        vertex_t u = current_node.vertex;
        dist_t d = current_node.min_dist;
        node_heap.pop();

        // this avoids considering nodes with out of date (ie not mininium) min distance
        if (d > min_dist[u])
            continue;

        const vector<neighbor> neighbors = graph.getAdjList(u);
        for (size_t i = 0; i < neighbors.size(); ++i)
        {
            vertex_t v = neighbors[i].vertex;
            dist_t d2 = neighbors[i].dist;
            dist_t dist_via_u = d + d2;
            if (dist_via_u < min_dist[v])
            {
                min_dist[v] = dist_via_u;
                previous[v] = u;
                node_heap.push(node(v, min_dist[v]));
            }
        }
    }
}

// Walk the previous vector to build the path backward - and reverse
dist_t Dijkstra::readShortestPath(vertex_t destination)
{
    vertex_t u, v;
    vector<vertex_t> rev_path_vertex;
    vector<dist_t> rev_path_dist;

    total_path_dist = 0;
    u = destination;
    while (u != source)
    {
        v = previous[u];
        if (v == -1)
        {
            total_path_dist = -1;
            return -1;
        }
        rev_path_vertex.push_back(u);
        rev_path_dist.push_back(graph.getAdjMatrix(u, v));
        u = v;
    }

    path_vertex.resize(rev_path_vertex.size());
    path_dist.resize(rev_path_dist.size());
    total_path_dist = 0;
    for (size_t i = 0; i < rev_path_vertex.size(); ++i)
    {
        path_vertex[i] = rev_path_vertex[rev_path_vertex.size() - 1 - i];
        path_dist[i] = rev_path_dist[rev_path_dist.size() - 1 - i];
        total_path_dist += path_dist[i];
    }
    return total_path_dist;
}

// display result
void Dijkstra::showPath()
{
    streamsize ss = cout.precision();
    cout << "Path" << endl;

    if (total_path_dist < 0)
    {
        cout << "No Path" << endl;
    }
    else
    {
        cout << "(" << source << ")";
        for (size_t i = 0; i < path_vertex.size(); i++)
        {
            cout << "(" << path_vertex.at(i) << ", " << setprecision(3) << path_dist.at(i) << ") ";
        }
        cout << endl;
        cout << "Total dist = " << total_path_dist << endl
             << "Done" << endl
             << endl;
    }

    cout.precision(ss);
}

// display raw algo raw output
void Dijkstra::showPrevious()
{
    streamsize ss = cout.precision();
    cout << "Previous" << endl;

    for (size_t i = 0; i < previous.size(); i++)
    {
        cout << "(" << i << ": " << previous.at(i) << ") ";
    }
    cout << endl;

    cout.precision(ss);
}
