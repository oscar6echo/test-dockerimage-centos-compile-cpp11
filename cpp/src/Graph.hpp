
#ifndef DEF_GRAPH
#define DEF_GRAPH

#include <vector>
#include <string>

#include "Types.hpp"

using namespace std;


class Graph
{
public:
    // default constructor
    Graph();

    // constructor for random graph
    Graph(int nbVertex, double densityEdge, double minEdgeDist, double maxEdgeDist);

    // constructor for graph in file
    Graph(string file_name);

    // desctructor
    ~Graph();

    // getters
    int getNbVertex();
    int getNbEdge();
    dist_t getMaxEdgeDist();

    // getters
    vector<neighbor> getAdjList(vertex_t u);
    dist_t getAdjMatrix(vertex_t u, vertex_t v);

    // display - debug functions
    void showAdjMatrix();
    void showAdjList();

    // load graph from file
    void loadGraph(string file_name);

private:
    // input in constructor - self explanatory
    int nbVertex;
    double densityEdge;
    double minEdgeDist;
    double maxEdgeDist;
    string file_name;

    // 2 graph representations
    // build in sync
    // each representation has a benefit
    // so both are used in Dijkstra.cpp via relevant getters
    vector< vector<neighbor> > adjList;
    vector< vector<dist_t> > adjMatrix;

    // effective building of random graph
    void buildRandomGraph();
};

#endif
