
#ifndef DEF_TYPES
#define DEF_TYPES

#include <queue>

using namespace std;

typedef int vertex_t;
typedef double dist_t;

// represents a vertex as a neighbor to another vertex with the (symmmetric) distance between the 2
struct neighbor
{
    vertex_t vertex;
    dist_t dist;

    neighbor(vertex_t arg_vertex, dist_t arg_dist) : vertex(arg_vertex), dist(arg_dist) {}
};

// represents a vertex with its distance from the source
class node
{
  public:
    vertex_t vertex;
    dist_t min_dist;

    node(vertex_t arg_vertex, dist_t arg_min_dist) : vertex(arg_vertex), min_dist(arg_min_dist) {}
};

// operator used to define a min heap - since C++ priority_queue is a max heap by default
class GreaterMinDist
{
  public:
    bool operator()(node lhs, node rhs)
    {
        return lhs.min_dist > rhs.min_dist;
    }
};

// is the min heap used to store and pop the candiate nodes to be considered next
typedef priority_queue<node, vector<node>, GreaterMinDist> min_heap;

#endif
