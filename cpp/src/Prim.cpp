
#include <iostream>
#include <iomanip>

#include "Graph.hpp"
#include "Prim.hpp"

using namespace std;

Prim::Prim(Graph arg_graph) : graph(arg_graph),
                              key(arg_graph.getNbVertex()),
                              mst(arg_graph.getNbVertex()),
                              previous(arg_graph.getNbVertex()){};

Prim::~Prim()
{
}

void Prim::init(vertex_t arg_source)
{
    // Maximum value for distance ~ INFINITY
    INFINITY = graph.getMaxEdgeDist() + 1;

    for (size_t u = 0; u < mst.size(); ++u)
    {
        // all vertices are set infinitely distant from MST
        key[u] = INFINITY;
        // all previous are unset
        previous[u] = -1;
        // MST is empty
        mst[u] = false;
    }

    // source initialisation
    source = arg_source;
    key[arg_source] = 0;
}

vertex_t Prim::closest_vertex_not_in_MST()
{
    dist_t m = INFINITY;
    vertex_t c;
    for (size_t u = 0; u < mst.size(); ++u)
    {
        if ((!mst[u]) && (key[u] < m))
        {
            c = u;
            m = key[u];
        }
    }
    return c;
}

// see https://en.wikipedia.org/wiki/Prim%27s_algorithm
void Prim::buildMST(vertex_t source)
{
    init(source);

    for (size_t i = 0; i < mst.size(); ++i)
    {
        vertex_t u = closest_vertex_not_in_MST();
        mst[u] = true;

        const vector<neighbor> neighbors = graph.getAdjList(u);
        for (size_t i = 0; i < neighbors.size(); ++i)
        {
            vertex_t v = neighbors[i].vertex;
            dist_t d = neighbors[i].dist;
            if (!mst[v] && d < key[v])
            {
                key[v] = d;
                previous[v] = u;
            }
        }
    }
}

dist_t Prim::readTotalDistMST()
{
    total_dist_MST = 0;
    for (size_t u = 0; u < mst.size(); ++u)
    {
        if (previous[u] >= 0)
        {
            dist_t c = graph.getAdjMatrix(u, previous[u]);
            total_dist_MST += c;
        }
    }
    return total_dist_MST;
}

// display raw algo raw output
void Prim::showPrevious()
{
    streamsize ss = cout.precision();
    cout << "Previous" << endl;

    for (size_t i = 0; i < previous.size(); i++)
    {
        cout << "(" << i << ": " << previous.at(i) << ") ";
    }
    cout << endl;

    cout.precision(ss);
}

// Walk the previous vector to build list all MST edges
// display result
void Prim::showMST()
{
    total_dist_MST = 0;

    cout << "Minimum Spanning Tree" << endl;
    cout << "vertex - vertex : cost" << endl;
    for (vertex_t u = 0; u < graph.getNbVertex(); ++u)
    {
        if (previous[u] >= 0)
        {
            dist_t c = graph.getAdjMatrix(u, previous[u]);
            total_dist_MST += c;
            cout << u << " - " << previous[u] << " : " << c << endl;
        }
    }
    cout << "Total Distance = " << total_dist_MST << endl
         << endl;
}
