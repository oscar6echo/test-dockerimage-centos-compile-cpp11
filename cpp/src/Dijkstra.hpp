
#ifndef DEF_DIJKSTRA
#define DEF_DIJKSTRA

#include <vector>

#include "Types.hpp"
#include "Graph.hpp"

using namespace std;

class Dijkstra
{
public:
    // constructor from a graph
    Dijkstra(Graph graph);

    // desctructor
    ~Dijkstra();

    // main member function determines the shortest path tree from vertex source
    void buildShortestPathTree(vertex_t source);

    // reads the path from source to destination from previous vector determined in function buildShortestPathTree
    // and returns the shortest path from source to destination
    dist_t readShortestPath(vertex_t destination);

    // show path from source to destination
    void showPath();

    // show previous vector containing the result of Dijkstra algo
    void showPrevious();

private:
    // graph object
    Graph graph;

    // source object
    vertex_t source;

    // min heap object storing all nodes considered during the algo
    // a node being (a vertex and its min distance to source)
    // a node may be stored more than once if is appear several times in the course of the algo
    // but only all distance > minimum will be discarded - see Dijkstra.cpp
    min_heap node_heap;

    // min distance for each vertex
    vector<dist_t> min_dist;

    // previous vertex for each vertex - built by the algo
    vector<vertex_t> previous;

    // the vertex path from source to destination
    vector<vertex_t> path_vertex;

    // the distance from vertex to vertex from source to destination
    vector<dist_t> path_dist;

    // the sum of path_dist
    dist_t total_path_dist;

    // helper function to initialize params before Dijkstra algo
    void init(vertex_t source);
};

#endif
