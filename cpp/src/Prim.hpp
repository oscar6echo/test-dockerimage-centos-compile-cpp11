
#ifndef DEF_PRIM
#define DEF_PRIM

#include <vector>

#include "Types.hpp"
#include "Graph.hpp"

using namespace std;

class Prim
{
public:
    // constructor from a graph
    Prim(Graph graph);

    // desctructor
    ~Prim();

    // main member function builds minimum spanning tree (MST)
    void buildMST(vertex_t source);

    // extract total MST distance from vector previous 
    dist_t readTotalDistMST();

    // show MST
    void showMST();

    // show previous vector containing the result of Prim algo
    void showPrevious();

private:
    // graph object
    Graph graph;

    // source vertex
    vertex_t source;

    // key is distance of vertex to MST: criteria for joining MST
    vector<dist_t> key;

    // vertex present in mst
    vector<bool> mst;

    // previous vertex for each vertex - built by the algo
    vector<vertex_t> previous;

    // the sum of MST edge distaances
    dist_t total_dist_MST;

    // helper function to initialize params before Prim algo
    void init(vertex_t source);

    // helper function to find the closet vertex to MST
    vertex_t closest_vertex_not_in_MST();

    // helper INFINITY relatve to edge distance
    dist_t INFINITY;
};

#endif
