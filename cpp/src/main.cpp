
#include <iostream>
#include <vector>

#include "Types.hpp"
#include "Graph.hpp"
#include "Dijkstra.hpp"
#include "Prim.hpp"

using namespace std;

// helper: average over numeric array
dist_t average(vector<dist_t> v)
{
    dist_t s = 0;
    for (size_t i = 1; i < v.size(); ++i)
    {
        s += v[i];
    }
    return s / v.size();
}

int main()
{
    // Sample Random Graph to exlore and debug
    cout << "Sample graph" << endl
         << endl;

    int nbVertex = 5;
    double densityEdge = 0.75;
    double minEdgeDist = 1.0;
    double maxEdgeDist = 10.0;

    Graph g = Graph(nbVertex, densityEdge, minEdgeDist, maxEdgeDist);

    g.showAdjMatrix();
    g.showAdjList();

    Dijkstra djk = Dijkstra(g);

    vertex_t source = 1;
    vertex_t destination = nbVertex - 1;

    djk.buildShortestPathTree(source);
    djk.readShortestPath(destination);
    djk.showPrevious();

    djk.showPath();

    // Assignement week3
    cout << "Simul graph" << endl
         << endl;

    // loop over nbGraph random graphs
    int nbGraph = 10;
    // each graph has nbVertex
    nbVertex = 50;
    // each edge in the graph has a densityEdge probability to exist
    densityEdge = 0.40;
    // each edge has a random distance - if it exists
    // between minEdgeDist
    minEdgeDist = 1.0;
    // end maxEdgeDist
    maxEdgeDist = 10.0;

    // contains the shortest paths determined by the Dijkstra algo for each graph & source/destination
    vector<dist_t> shortest_path;
    dist_t avg_sp;

    for (int k = 0; k < nbGraph; ++k)
    {
        // genrate random graph
        Graph g2 = Graph(nbVertex, densityEdge, minEdgeDist, maxEdgeDist);
        // g2.showAdjMatrix();
        // g2.showAdjList();

        Dijkstra djk2 = Dijkstra(g2);
        source = 0;
        //  compute shortest path tree
        djk2.buildShortestPathTree(source);

        for (int i = 1; i < nbVertex; ++i)
        {
            // for all destinations not source compute shortest path
            dist_t sp = djk2.readShortestPath(i);
            if (sp > 0)
            {
                shortest_path.push_back(sp);
            }
        }
    }

    // average all shortest paths
    avg_sp = average(shortest_path);

    // results
    cout << "Params:" << endl;
    cout << "nbGraph = " << nbGraph << ", nbVertex = " << nbVertex << ", densityEdge = " << densityEdge;
    cout << ", min/maxEdgeDist = " << minEdgeDist << " / " << maxEdgeDist << endl;
    cout << "Results:" << endl;
    cout << "Average Shortest Path = " << avg_sp << " over " << shortest_path.size() << " paths" << endl
         << endl;

    // Assignement week4
    // load graph
    cout << "Loaded graph" << endl
         << endl;
    Graph g3 = Graph("./data/Homework3_SampleTestData_mst_data.txt");
    // show graph
    g3.showAdjMatrix();
    g3.showAdjList();

    Prim pri = Prim(g3);

    // any source will give the same MST total distance
    source = 0;
    //  compute MST
    pri.buildMST(source);
    pri.showPrevious();
    pri.showMST();

    // check that all source give same MST total distance
    cout << "source : MST total dist " << endl;
    for (vertex_t u = 0; u < g3.getNbVertex(); ++u)
    {
        pri.buildMST(u);
        cout << u << " : " << pri.readTotalDistMST() << endl;
    }
}
