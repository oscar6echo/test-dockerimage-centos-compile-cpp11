
#include <random>
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <iterator>


#include "Graph.hpp"

using namespace std;

Graph::Graph() : nbVertex(10),
                 densityEdge(0.1),
                 minEdgeDist(1.0),
                 maxEdgeDist(100.0),
                 adjList(10),
                 adjMatrix(10)
{
    buildRandomGraph();
};

Graph::Graph(int arg_nbVertex,
             double arg_densityEdge,
             dist_t arg_minEdgeDist,
             dist_t arg_maxEdgeDist) : nbVertex(arg_nbVertex),
                                       densityEdge(arg_densityEdge),
                                       minEdgeDist(arg_minEdgeDist),
                                       maxEdgeDist(arg_maxEdgeDist),
                                       adjList(arg_nbVertex),
                                       adjMatrix(arg_nbVertex)
{
    buildRandomGraph();
};

Graph::~Graph()
{
}

Graph::Graph(string arg_file_name) : file_name(arg_file_name)
{
    ifstream data_file(file_name);
    istream_iterator<int> start(data_file), end;
    vector<int> data(start, end);

    if (data.size() == 0)
    {
        cout << "ERROR" << endl;
        cout << "file " << file_name << "does not exist" << endl;
    }

    nbVertex = data[0];

    adjMatrix.resize(nbVertex, vector<dist_t>(nbVertex, 0));
    // for (int u = 0; u < nbVertex; ++u)
    // {
    //     adjMatrix[u].resize(nbVertex, 0);
    // }

    for (auto e = data.begin() + 1; e != data.end(); e = e + 3)
    {
        // cout << *e << endl;
        vertex_t u = *e;
        vertex_t v = *(e + 1);
        dist_t c = *(e + 2);
        adjMatrix[u][v] = c;
    }

    // check if adjMatrix is symmetric / has 'reflexive' vertices
    bool is_sym = true;
    bool has_diag = false;
    for (int u = 0; u < nbVertex; ++u)
    {
        for (int v = u + 1; v < nbVertex; ++v)
        {
            if (adjMatrix[u][v] != adjMatrix[v][u])
            {
                is_sym = false;
            }
        }
        for (int u = 0; u < nbVertex; ++u)
        {
            if (adjMatrix[u][u] != 0)
            {
                has_diag = true;
            }
        }
    }
    cout << "Note: graph in file is " << (is_sym ? " " : " not ") << "symmetric" << endl;
    cout << "Note: graph has " << (has_diag ? " " : "no ") << "vertices connected to themselves" << endl;

    // build adjList
    adjList.resize(nbVertex);
    for (int u = 0; u < nbVertex; ++u)
    {
        for (int v = u + 1; v < nbVertex; ++v)
        {
            if (adjMatrix[u][v] > 0)
            {
                dist_t c = adjMatrix[u][v];
                adjList[u].push_back(neighbor(v, c));
                adjList[v].push_back(neighbor(u, c));
            }
        }
    }

    cout << "nbVertex = " << nbVertex << ", nbEdge = " << getNbEdge() << endl
         << endl;
}

void Graph::buildRandomGraph()
{
    random_device rd;  //Will be used to obtain a seed for the random number engine
    mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    uniform_real_distribution<> distrib_isEdge(0, 1);
    uniform_real_distribution<> distrib_Dist(minEdgeDist, maxEdgeDist);

    double r, c;

    for (int u = 0; u < nbVertex; ++u)
    {
        adjMatrix[u].resize(nbVertex, 0);
    }

    for (int u = 0; u < nbVertex; ++u)
    {
        for (int v = u; v < nbVertex; ++v)
        {
            if (v > u)
            {
                r = distrib_isEdge(gen);
                if (r < densityEdge)
                // top right triangle of adjacency matrix
                // and symmetric bottom left triangle
                // random data generated if random < density
                {
                    c = distrib_Dist(gen);
                    adjMatrix[u][v] = adjMatrix[v][u] = c;
                    adjList[u].push_back(neighbor(v, c));
                    adjList[v].push_back(neighbor(u, c));
                }
                else
                // diagonal
                // no data for adjacency list as no 'reflexive edge'
                // matrix point added for easier visualisation
                {
                    adjMatrix[u][v] = adjMatrix[v][u] = 0;
                }
            }
            else
            {
                adjMatrix[u][v] = 0;
            }
        }
    }
}

dist_t Graph::getMaxEdgeDist()
{
    dist_t m = 0;

    for (int u = 0; u < nbVertex; ++u)
    {
        // only top right triange is considered
        for (int v = u + 1; v < nbVertex; ++v)
        {
            double c = adjMatrix[u][v];
            m = (c > m) ? c : m;
        }
    }
    return m;
}

int Graph::getNbEdge()
{
    int n = 0;

    for (int u = 0; u < nbVertex; ++u)
    {
        // only top right triange is considered
        for (int v = u + 1; v < nbVertex; ++v)
        {
            if (adjMatrix[u][v] > 0)
            {
                n++;
            }
        }
    }
    return n;
}

int Graph::getNbVertex()
{
    return nbVertex;
}

vector<neighbor> Graph::getAdjList(vertex_t u)
{
    return adjList[u];
}

dist_t Graph::getAdjMatrix(vertex_t u, vertex_t v)
{
    return adjMatrix[u][v];
}

// display - debug: adjacency matrix
void Graph::showAdjMatrix()
{
    // important to reset default precision at the end of function
    streamsize ss = cout.precision();

    cout << "Graph with " << nbVertex << " vertices and " << getNbEdge() << " edges" << endl
         << endl;

    cout << "Adjacency Matrix" << endl;

    cout << "   ";
    for (size_t u = 0; u < adjMatrix.size(); u++)
    {
        cout << setw(6) << u << " ";
    }
    cout << endl
         << endl;

    for (size_t u = 0; u < adjMatrix.size(); u++)
    {
        cout << u << ": ";
        for (size_t v = 0; v < adjMatrix[u].size(); v++)
        {
            // setw to display a number over a fixed number of digits
            // setprecision to round number
            cout << setw(6) << setprecision(4) << adjMatrix.at(u).at(v) << " ";
        }
        cout << endl;
    }
    cout << "Done" << endl
         << endl;
    cout.precision(ss);
};

// display - debug: adjacency lists
void Graph::showAdjList()
{
    // important to reset default precision at the end of function
    streamsize ss = cout.precision();
    cout << "Adjacency List" << endl;

    for (size_t u = 0; u < adjList.size(); u++)
    {
        cout << u << ": ";
        for (size_t v = 0; v < adjList[u].size(); v++)
        {
            cout << "(" << setprecision(4) << adjList.at(u).at(v).vertex << ", " << adjList.at(u).at(v).dist << ") ";
        }
        cout << endl;
    }
    cout << "Done" << endl
         << endl;
    cout.precision(ss);
};
