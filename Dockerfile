
# Docker Cloud image name: oscar6echo/centos-gcc-miniconda
# To retrieve: "docker pull oscar6echo/centos-gcc-miniconda"

FROM centos:latest

MAINTAINER Olivier Borderies

# load packages
RUN yum upgrade -y && \
    # standard tools
    yum install -y wget bzip2 locales ca-certificates which && \
    # dev tools
    yum groupinstall -y "Development Tools" --setopt=group_package_types=mandatory,default,optional && \
    yum clean all

# set locale
RUN localedef -i en_US -f UTF-8 en_US.UTF-8


# install tini
RUN TINI_VERSION=`curl https://github.com/krallin/tini/releases/latest | grep -o "/v.*\"" | sed 's:^..\(.*\).$:\1:'` && \
    wget --quiet https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini && \
    mv tini /usr/local/bin/tini && \
    chmod +x /usr/local/bin/tini

# configure environment
ENV CONDA_DIR /opt/conda
ENV PATH $CONDA_DIR/bin:$PATH
ENV NB_USER oscar6echo
ENV NB_UID 1000
ENV HOME /home/$NB_USER
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8


# Create user oscar6echo with UID=1000 and in the 'users' group
RUN useradd -m -s /bin/bash -N -u $NB_UID $NB_USER && \
    mkdir -p $CONDA_DIR && \
    chown $NB_USER $CONDA_DIR

USER $NB_USER

# setup oscar6echo home directory
RUN mkdir /home/$NB_USER/.jupyter && \
    echo "cacert=/etc/ssl/certs/ca-certificates.crt" > /home/$NB_USER/.curlrc


# install miniconda as oscar6echo
RUN wget --quiet https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -f -b -p $CONDA_DIR && \
    rm ~/miniconda.sh

RUN $CONDA_DIR/bin/conda config --system --add channels conda-forge && \
    $CONDA_DIR/bin/conda config --system --set auto_update_conda false && \
    conda clean -tipsy

# install jupyter notebook and jupyterlab
RUN conda install -y \
    'notebook=5.0.*' \
    'jupyterlab=0.19.*' \
    && conda clean -tipsy


USER root

# set root password - debug only as insecure
RUN echo root:toto | chpasswd

# jupyter auth
COPY jupyter_notebook_config.py /home/$NB_USER/.jupyter/
RUN chown -R $NB_USER:users /home/$NB_USER/.jupyter

# container starts as oscar6echo - by default
USER $NB_USER

# configure container startup
WORKDIR /home/$NB_USER
ENTRYPOINT ["tini", "--"]
EXPOSE 8888

CMD [ "/bin/bash" ]

