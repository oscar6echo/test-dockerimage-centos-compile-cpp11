
#### Build an image from directory containing Dockerfile
````
docker build -t centos-miniconda .
````

#### Remove all exited containers
````
docker rm $(docker ps -qf  status=exited)
````

#### Run image in interactive mode
````
docker run -it centos-miniconda
````

#### Run image in interactive mode as root
````
docker run -u 0 -it centos-miniconda
````

#### Run image in interactive mode & expose port 8888
````
docker run -it -p 8888:8888 centos-miniconda
````

#### Run image in interactive mode & expose port 8888 & mount host folder inside container
````
docker run -it -v /Users/Olivier/Dropbox/Archives/Software/Docker:/home/oscar6echo/Docker centos-miniconda
````

